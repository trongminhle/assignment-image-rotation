//
// Created by trongminhle on 03/11/21.
//
#ifndef ROTATE_IMAGE_H
#define ROTATE_IMAGE_H

#include <stdbool.h>
#include <stdint.h>


struct image { uint32_t width, height;
    struct pixel *sum_pixel_of_image;
};

struct pixel {
    uint8_t b, g, r;
};

bool image_malloc_data(struct image *image_store);

void free_img(struct image *image_store);

#endif //ROTATE_IMAGE_H

