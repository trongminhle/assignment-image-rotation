/* создаёт копию изображения, которая повёрнута на 90 градусов */

#include <mm_malloc.h>

#include "picture.h"

struct image rotate( struct image const source );
