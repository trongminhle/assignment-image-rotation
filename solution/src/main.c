//
// Created by trongminhle on 05/11/21.
//

#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/rotate.h"



int main(int argc, char **argv) {
    // too much args
    if (argc != 3) {
        return 1;
    }
    char *input_fn = NULL, *output_fn = NULL;
    input_fn = argv[1];
    output_fn = argv[2];


    FILE *input_file = NULL;
    FILE *output_file = NULL;

    // open err
    if (check_open_file(&input_file, input_fn, "rb")){
        return 1;
    }
    if (check_open_file(&output_file, output_fn, "wb")){
        return 1;
    }

    struct image image = {0};
    // convert err
    if (from_bmp(input_file, &image)){
        return 1;
    }

    struct image new_image = rotate(image);
    // write err
    if (to_bmp(output_file, &new_image)){
        return 1;
    }

    // checking close file, close err
    if (check_close_file(&input_file)){
        return 1;
    }
    if (check_close_file(&output_file)){
        return 1;
    }

    // release memory
    free_img(&new_image);
    free_img(&image);
    return 0;
}
