//
// Created by trongminhle on 03/11/21.
//

#include "../include/picture.h"

#include <malloc.h>

// dynamically allocate memory to store image

bool image_malloc_data(struct image *image_store){
    uint32_t width = image_store->width;
    uint32_t height = image_store->height;
    image_store->sum_pixel_of_image = malloc(sizeof(struct pixel) * width * height);
    return image_store->sum_pixel_of_image == NULL;
}

// release memory
void free_img(struct image *image_store){ free(image_store->sum_pixel_of_image); }
