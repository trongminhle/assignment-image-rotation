//
// Created by trongminhle on 05/11/21.
//
#include "../include/file.h"

bool check_open_file(FILE **file, const char *name, const char *mode) {
    if (!file) {
        return false;
    }
    *file = fopen(name, mode);
    return *file == NULL;
}

bool check_close_file(FILE **file) {
    if (*file) {
        return false;
    }
    return fclose(*file);
}
